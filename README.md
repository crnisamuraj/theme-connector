<div align="center">
<h1 align="center">
  <img src="https://gitlab.com/uploads/-/system/project/avatar/18677788/icon.png" alt="Project">
  <br />
  Theme Connector For Snaps
</h1>
</div>

GTK snaps which use strict confinement do not inherit the look of custom themes 
which are not included in the gtk-common-themes snap (installed by default).

This program allows you to connect the snap apps which have available plugs to
gtk-common-themes:gtk-3-themes to other custom themes downloaded from the Snap
Store, e.g. adapta-gtk-snap, qogir-theme-snap and vimix-gtk-themes-snap.

## How the program works

The Python script runs a few shell commands:

(get a list of installed snap custom themes)
```
snap list | grep -E '(theme-snap|themes-snap|adapta-gtk-snap)' | awk '{print $1}'
```

(check which apps are connected to gtk-common-themes)
```
snap connections gtk-common-themes | grep gtk-3-themes | awk '{print $2}'
```

Then the GUI allows you to select an installed custom theme and connect other
snaps to it by running*:


```
echo [pass] | sudo -S snap connect [snap_app]:gtk-3-themes [theme_name]:gtk-3-themes
```
*(```snap connect``` needs sudo, therefore a popup window asks you for authentication (the [pass] variable))

## GUI screenshots

<img src="https://gitlab.com/gantonayde/theme-connector/-/raw/master/connect-window.png" alt="Connect Window"> 

<img src="https://gitlab.com/gantonayde/theme-connector/-/raw/master/popup-window.png" alt="Popup Window"> 